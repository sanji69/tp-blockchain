// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract Wedding {
    string firstPartner;
    string secondPartner;      
    uint startDate;

    function setWedding(string memory first, string memory second) public {
        firstPartner = first;
        secondPartner = second;
        startDate = block.timestamp;
    }

    function getWedding() public view returns (string memory first, string memory second, uint start) {
        return (firstPartner, secondPartner, startDate);
    }
}